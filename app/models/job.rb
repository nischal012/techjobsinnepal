class Job < ActiveRecord::Base

	validates :title, presence: true
	validates :location, presence: true
	validates :description, presence: true
	validates :comp_name, presence: true
	validates :comp_url, presence: true
	validates :email_comp, presence: true
	validates :email_customer, presence: true

	#scope for searching ==>  index page
	scope :search, -> (keyword){ where('keywords LIKE ?',"%#{keyword.downcase}%") if keyword.present?}
	scope :filter, ->(filter) {where('category_id like ?',"%#{filter}") if filter.present? }

	#filtering categories ==> index page doesnt works right now.
   # scope :filter, -> (category_id){ joins(:categories).where('categories.id=?',category_id)}

	before_save :set_keywords

	protected
	def set_keywords
		self.keywords=[title,description,comp_name].map(&:downcase).join(' ')
	end

	belongs_to :category, dependent: :destroy

end


