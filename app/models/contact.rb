class Contact < ActiveRecord::Base
    validates :title, presence: true
    validates :description, presence: true
    validates :email, presence: true,format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }

end
